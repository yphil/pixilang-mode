# Pixilang mode

GNU [Emacs](https://www.gnu.org/software/emacs/) minor mode for editing [Pixilang](http://www.warmplace.ru/soft/pixilang) code ; See my album "[Animal Panoptics](https://bitbucket.org/yassinphilip/animal-panoptics)".

![screenshot](https://bitbucket.org/yassinphilip/pixilang-mode/raw/master/screenshot.png)

## Installation

Clone it directly in your load-path :

```sh
# cd ~/.emacs.d/elisp/
# git clone https://github.com/yassinphilip/pixilang-mode.git
```

Make sure your load-path is recursive:

(~/.emacs)
```lisp
(let ((default-directory "~/.emacs.d/elisp/"))
  (normal-top-level-add-subdirs-to-load-path))
```

And require it:

(~/.emacs)
```lisp
(require 'pixilang-mode nil 'noerror)
```

## Usage

```lisp
M-x pixilang-mode
```

To automatically load pixilang-mode when you visit a .pixi file, put this in your Emacs init file:

```lisp
(add-to-list 'auto-mode-alist '("\\.pixi\\'" . pixilang-mode))
```